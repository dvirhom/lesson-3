#include "Vector.h"
#include <iostream>


Vector::Vector(int n)
{
	if (n < MIN_ARG)
	{
		n = 2;
	}
	Vector::_elements = (int*)std::malloc(sizeof(int)*n);
	Vector::_resizeFactor = n;
	Vector::_capacity = n;
	Vector::_size = 0;
}


Vector::~Vector()
{
	std::free(Vector::_elements);
	Vector::_elements = nullptr;
}

int Vector::size()const
{
	return Vector::_size;
}

int Vector::capacity()const
{
	return Vector::_capacity;
}

int Vector::resizeFactor()const
{
	return Vector::_resizeFactor;
}

bool Vector::empty()const
{
	return Vector::_size == 0;
}

void Vector::push_back(const int& val)
{
	Vector::_size++;
	if (Vector::size() < Vector::capacity())
	{
		Vector::_elements[Vector::_size - 1] = val;
	}
	else
	{
		Vector::_capacity += Vector::_resizeFactor;
		Vector::_elements = (int*)std::realloc(Vector::_elements, (Vector::_capacity) * sizeof(int));
		Vector::_elements[Vector::_size - 1] = val;
	}

}

int Vector::pop_back()
{
	int ans = 0;
	if (Vector::empty())
	{
		std::cout << "error: pop from empty vector" << std::endl;
		return ERROR;
	}
	else
	{
		ans = Vector::_elements[Vector::size() - 1];
		Vector::_elements[Vector::size() - 1] = 0;
		this->Vector::_size--;
	}
	return ans;
}

void Vector::reserve(int n)
{
	if (Vector::_capacity < n)
	{
		while (Vector::_capacity < n)
		{
			Vector::_capacity += Vector::_resizeFactor;
		}
		Vector::_elements = (int*)std::realloc(Vector::_elements, (Vector::_capacity) * sizeof(int));
	}
}

void Vector::resize(int n)
{ 
	if (Vector::capacity() > n)
	{
		Vector::_capacity = n;
		Vector::_elements = (int*)std::realloc(Vector::_elements, (Vector::capacity()) * sizeof(int));
		Vector::_size = n;
	}
	else
	{
		reserve(n);//make the capicity bigger
	}

}

void Vector::assign(int val)
{
	int i = 0;
	for (i = 0; i < Vector::size(); i++)
	{
		Vector::_elements[i] = val;
	}
}

void Vector::resize(int n, const int & val)
{
	Vector::resize(n);
	Vector::assign(val);
}

Vector::Vector(const Vector & other)
{
	*this = other;//use operator=
}

Vector & Vector::operator=(const Vector & other)
{
	int i = 0;
	if (this->_elements == other._elements)//if tries to copy the object to itself
	{
		return *this;
	}
	this->_capacity = other.capacity();
	this->_resizeFactor = other.resizeFactor();
	this->_size = other.size();
	std::free(this->_elements);//to create new array with same size and capicity as other 
	this->_elements = (int*)std::malloc(other.capacity() * sizeof(int));
	for (i = 0; i < this->_size; i++)
	{
		this->_elements[i] = other._elements[i];// copies cell by cell
	}
	return *this;
}

int & Vector::operator[](int n) const
{
	int ans = this->_elements[0];
	if (n < this->size() && n >= 0)//check if the index is valid
	{
		ans = this->_elements[n];
	}
	return ans;
}
void main()
{
	Vector ad(4);
	ad.push_back(12);
	ad.push_back(11);
	ad.push_back(10);
	ad.push_back(9);
	printf("%d ", ad[10]);
	printf("%d ", ad[10]);
	printf("%d ", ad[2]);
	printf("%d ", ad[3]);
	printf("%d ", ad[4]);
	printf("%d ", ad[20]);

	getchar();
}



